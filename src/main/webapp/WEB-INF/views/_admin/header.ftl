<header class="main-header">
    <!-- Logo -->
    <a href="/index.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">Tea</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">TeaCMS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">切换导航</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="http://www.miued.com/wp-content/themes/miued/img/logo.png" class="user-image" alt="User Image">
                        <span class="hidden-xs">${user.userNicename}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="http://www.miued.com/wp-content/themes/miued/img/logo.png" class="img-circle" alt="User Image">

                            <p>
                            ${user.userNicename}
                            </p>
                        </li>
                        <!-- Menu Body -->
       <#--                 <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                            <!-- /.row &ndash;&gt;
                        </li>-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <button class="btn btn-default btn-flat" data-toggle="modal" data-target="#myModal">修改密码</button>
                            </div>
                            <div class="pull-right">
                                <a id="logout" href="/admin/logout" class="btn btn-default btn-flat">退出</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    修改密碼
                </h4>
            </div>
            <form id="alertPass">
                <div class="modal-body">
                        <div class="form-group">
                            <label for="psaa1">原密码</label>
                            <input type="password" class="form-control" id="psaa1" name="psaa1" placeholder="请输入原密码">
                        </div>
                        <div class="form-group">
                            <label for="psaa2">密码</label>
                            <input type="password" class="form-control" id="psaa2" name="psaa2" placeholder="请输入密码">
                        </div>
                        <div class="form-group">
                            <label for="psaa3">确认密码</label>
                            <input type="password" class="form-control" id="psaa3" name="psaa3" placeholder="请输入确认密码">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-success">修改</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>