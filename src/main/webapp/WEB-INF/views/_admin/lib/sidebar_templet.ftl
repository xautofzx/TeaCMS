<#macro sidebar urlName>
<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header text-center">导航</li>
            <li <#if urlName == 'main'>class="active"</#if>>
                <a href="/admin/index.html">
                    <i class="fa fa-dashboard"></i> <span>仪表盘</span>
                    <span class="pull-right-container">
                  <#--<small class="label pull-right bg-green">Hot</small>-->
                </span>
                </a>
            </li>
            <li <#if urlName == 'article-all'>class="active"</#if>>
                <a href="/admin/article-all.html">
                    <i class="fa fa-google-wallet"></i> <span>所有文章</span>
                </a>
            </li>
            <li <#if urlName == 'article-new'>class="active"</#if>>
                <a href="/admin/article-new.html">
                    <i class="fa fa-edit"></i> <span>写文章</span>
                </a>
            </li>
            <#if user.userRight == 1>
            <li <#if urlName == 'banner'>class="active"</#if>>
                <a href="/admin/banner.html">
                    <i class="fa fa-map-o"></i> <span>轮播</span>
                </a>
            </li>
            </#if>
            <li <#if urlName == 'article-category'>class="active"</#if>>
                <a href="/admin/article-category.html">
                    <i class="fa fa-columns"></i> <span>分类</span>
                </a>
            </li>
            <li <#if urlName == 'article-tags'>class="active"</#if>>
                <a href="/admin/article-tags.html">
                    <i class="fa fa-tags"></i> <span>标签</span>
                </a>
            </li>
            <#if user.userRight == 1>
            <li <#if urlName == 'user-manage'>class="active"</#if>>
                <a href="/admin/user-manage.html">
                    <i class="fa fa-user"></i> <span>用户管理</span>
                </a>
            </li>
            <li <#if urlName == 'theme'>class="active"</#if>>
                <a href="/admin/theme.html">
                    <i class="fa fa-television"></i> <span>主题</span>
                </a>
            </li>
            <li <#if urlName == 'druid'>class="active"</#if>>
                <a href="/admin/druid.html">
                    <i class="fa fa-database"></i> <span>数据监控</span>
                </a>
            </li>
            <li <#if urlName == 'monitoring'>class="active"</#if>>
                <a href="/admin/monitoring.html">
                    <i class="fa fa-bar-chart"></i> <span>性能监控</span>
                </a>
            </li>
            <li <#if urlName == 'duoshuo'>class="active"</#if>>
                <a href="/admin/duoshuo.html">
                    <i class="fa fa-comment-o"></i> <span>多说</span>
                </a>
            </li>
            <li <#if urlName == 'config'>class="active"</#if>>
                <a href="/admin/config.html">
                    <i class="fa fa-cog"></i> <span>设置</span>
                </a>
            </li>
            </#if>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->
</#macro>