package me.teacms.controller.admin;

import me.teacms.common.result.JsonResult;
import me.teacms.dao.ArticleAllDao;
import me.teacms.entity.User;
import me.teacms.entity.vo.SearchInfo;
import me.teacms.service.ArticleAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/21
 * Time: 19:37
 * Describe: 所有文章
 */
@Controller
@RequestMapping(value = "/admin")
public class ArticleAllCotroller {

    @Autowired
    private ArticleAllService articleAllService;

    /**
     * 所有文章视图
     *
     * @return
     */
    @RequestMapping(value = "/article-all.html")
    public String articleAll() {

        return "_admin/article/article_all";
    }

    /**
     * 查询所有文章 并使用PageHelp分页
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getallarticleinfo")
    public JsonResult getAllArticleInfo(HttpSession session, @RequestBody SearchInfo searchInfo) {

        /**
         * 加入用户信息
         */
        User user = (User) session.getAttribute("user");
        searchInfo.setUser(user);
        JsonResult articleAll = articleAllService.findArticleAll(searchInfo);

        return articleAll;
    }

    /**
     * 回收站 trashCan
     * 查询所有回收站文章 并 INNER JOIN 文章作者名
     * 传入文章名
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getalltrashcanarticle")
    public JsonResult getAllTrashCanArticle(HttpSession session, @RequestBody SearchInfo searchInfo) {

        /**
         * 加入用户信息
         */
        User user = (User) session.getAttribute("user");
        searchInfo.setUser(user);
        JsonResult articleAll = articleAllService.findAllTrashCanArticle(searchInfo);

        return articleAll;
    }

    /**
     * 逻辑删除文章
     * 更新文章状态
     *
     * @param id 传入文章ID
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deletelogicarticle")
    public JsonResult deleteLogicArticle(@RequestBody Integer id) {

        JsonResult jsonResult = articleAllService.deleteLogicArticle(id);

        return jsonResult;
    }

    /**
     * 恢复文章到所有文章中
     *
     * @param id 传入文章ID
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/recoverarticle")
    public JsonResult recoverArticle(@RequestBody Integer id) {

        JsonResult jsonResult = articleAllService.updateArticleState(id);

        return jsonResult;
    }

    /**
     * 彻底删除文章
     *
     * @param id 传入文章ID
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deletearticle")
    public JsonResult deleteArticle(@RequestBody Integer id) {

        JsonResult jsonResult = articleAllService.deleteArticle(id);

        return jsonResult;
    }

}
