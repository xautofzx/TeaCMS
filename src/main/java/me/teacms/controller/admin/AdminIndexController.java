package me.teacms.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/20
 * Time: 23:57
 * Describe: 仪表盘主页
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminIndexController {

    /**
     * 仪表盘信息
     * @return
     */
    @RequestMapping(value = "/index.html")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView();
        view.setViewName("/_admin/index");

        return  view;
    }

}
