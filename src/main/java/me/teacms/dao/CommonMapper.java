package me.teacms.dao;

import me.teacms.entity.Category;
import me.teacms.entity.Tag;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 17:08
 * Describe: 通用Mapper
 */
public interface CommonMapper {

    /**
     * 查询所有分类
     * @return
     */
    public List<Category> findAllCategory();

    /**
     * 查询所有标签
     * @return
     */
    public List<Tag> findAllTag();

}
