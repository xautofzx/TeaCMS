package me.teacms.dao;

import me.teacms.entity.Tag;
import java.util.List;

public interface TagMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Tag record);

    int insertSelective(Tag record);

    Tag selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Tag record);

    int updateByPrimaryKey(Tag record);

    //利用pageHelper查询标签信息
    List<Tag> selectAllTagsInfos();

    //多表删除标签
    int deleteTag(Integer id);
}