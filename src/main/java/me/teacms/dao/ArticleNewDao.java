package me.teacms.dao;

import me.teacms.entity.ArticleCategory;
import me.teacms.entity.ArticleTag;
import me.teacms.entity.vo.ArticleNewVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 21:26
 * Describe: 添加文章
 */
public interface ArticleNewDao {

    /**
     * 添加文章
     * @param articleNewVo
     * @return
     */
    int addArticle(ArticleNewVo articleNewVo);

    /**
     * 添加文章分类
     * @param articleCategoryNews
     * @return
     */
    int addArticleCategory(List<ArticleCategory> articleCategoryNews);

    /**
     * 添加文章标签
     * @param articleTagsNews
     * @return
     */
    int addArticleTag(ArrayList<ArticleTag> articleTagsNews);
}
