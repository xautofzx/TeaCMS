package me.teacms.dao;

import me.teacms.entity.ArticleCategory;
import me.teacms.entity.ArticleTag;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.entity.vo.ArticleNewVo;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/27
 * Time: 17:05
 * Describe: 编辑文章
 */
public interface ArticleEditDao {

    /**
     * 查询文章
     * 传入文章ID 和 用户 ID
     * @param id
     * @return
     */
    ArticleAllVo findArticById(Integer id);

    /**
     * 删除文章分类
     * @param value
     * @return
     */
    int deleteArticleCategorie(Integer value);

    /**
     * 删除文章标签
     * @param value
     * @return
     */
    int deleteArticleTag(Integer value);

    /**
     * 更新文章
     * @param articleNewVo
     * @return
     */
    int updateArticle(ArticleNewVo articleNewVo);

}
