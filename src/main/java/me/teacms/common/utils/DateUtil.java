package me.teacms.common.utils;

import java.util.Calendar;

/**
 * 用于获取当前 年 月
 * @author XiaoBingBy
 */
public class DateUtil {
	
	/**
	 * 获取年份
	 * @return
	 */
	public static int getYear() {

		Calendar cal = Calendar.getInstance();
		
		return  cal.get(Calendar.YEAR);
	}
	
	/**
	 * 获取日期
	 * @return
	 */
	public static int getMonth() {
		Calendar cal = Calendar.getInstance();
		
		return  cal.get(Calendar.MONTH)+1;		
	}
	
}
