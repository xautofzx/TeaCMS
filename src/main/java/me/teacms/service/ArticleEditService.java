package me.teacms.service;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.entity.vo.ArticleNewVo;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/27
 * Time: 17:17
 * Describe: 编辑文章业务接口
 */
public interface ArticleEditService {

    /**
     * 查询文章
     * 传入文章ID 和 用户 ID
     * @return
     */
    public ArticleAllVo findArticById(Integer id);

    /**
     * 更新文章
     * @param articleNewVo
     * @return
     */
    public JsonResult updateArticle(ArticleNewVo articleNewVo);

}
