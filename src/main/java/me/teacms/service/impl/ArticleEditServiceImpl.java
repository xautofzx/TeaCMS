package me.teacms.service.impl;

import me.teacms.common.result.JsonResult;
import me.teacms.dao.ArticleAllDao;
import me.teacms.dao.ArticleEditDao;
import me.teacms.dao.ArticleMapper;
import me.teacms.dao.ArticleNewDao;
import me.teacms.entity.ArticleCategory;
import me.teacms.entity.ArticleTag;
import me.teacms.entity.Category;
import me.teacms.entity.Tag;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.entity.vo.ArticleNewVo;
import me.teacms.service.ArticleEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/27
 * Time: 17:17
 * Describe: 编辑文章业务接口实现
 */
@Service
public class ArticleEditServiceImpl implements ArticleEditService {

    @Autowired
    private ArticleAllDao articleAllDao;
    @Autowired
    private ArticleEditDao articleEditDao;
    @Autowired
    private ArticleNewDao articleNewDao;

    /**
     * 查询文章
     * 传入文章ID 和 用户 ID
     * @return
     */
    @Override
    public ArticleAllVo findArticById(Integer id) {

        ArticleAllVo article = articleEditDao.findArticById(id);

        /**
         * 查询文章标签
         * 查询文章
         */
        article.setCategories(articleAllDao.findAllCategory(id));
        article.setTags(articleAllDao.findAllTag(id));


        return article;
    }

    /**
     * 更新文章
     * @param articleNewVo
     * @return
     */
    @Override
    public JsonResult updateArticle(ArticleNewVo articleNewVo) {

        /**
         * 删除文章分类
         * 删除文章标签
         */
        articleEditDao.deleteArticleCategorie(articleNewVo.getId());
        articleEditDao.deleteArticleTag(articleNewVo.getId());

        /**
         * 插入文章分类
         */
        List<ArticleCategory> articleCategories = articleNewVo.getArticleCategories();
        if (articleCategories == null) {
            ArticleCategory item = new ArticleCategory();
            item.setArticleId(articleNewVo.getId());
            item.setCategoryId(1);
            ArrayList<ArticleCategory> temp = new ArrayList<>();
            temp.add(item);
            int addArticleCategoryState = articleNewDao.addArticleCategory(temp);
        } else {
            List<ArticleCategory> articleCategoryNews = new ArrayList<ArticleCategory>();
            for (ArticleCategory item : articleCategories) {
                if (item.getCategoryId() != null) {
                    item.setArticleId(articleNewVo.getId());
                    articleCategoryNews.add(item);
                }
            }
            int addArticleCategoryState = articleNewDao.addArticleCategory(articleCategoryNews);
        }


        /**
         * 插入文章标签
         */
        List<ArticleTag> articleTags = articleNewVo.getArticleTags();
        if (articleTags != null) {
            ArrayList<ArticleTag> articleTagsNews = new ArrayList<>();
            for (ArticleTag item : articleTags) {
                if (item.getTagId() != null) {
                    item.setArticleId(articleNewVo.getId());
                    articleTagsNews.add(item);
                }
            }
            int addArticleTagState = articleNewDao.addArticleTag(articleTagsNews);
        }

        /**
         * 更新文章
         */
        int i = articleEditDao.updateArticle(articleNewVo);

        JsonResult jsonResult = new JsonResult();
        jsonResult.setObj(1);
        jsonResult.setSuccess(true);

        return jsonResult;
    }
}
