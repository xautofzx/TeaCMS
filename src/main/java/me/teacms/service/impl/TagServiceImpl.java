package me.teacms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import me.teacms.common.result.JsonResult;
import me.teacms.dao.TagMapper;
import me.teacms.entity.Tag;
import me.teacms.entity.User;
import me.teacms.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: xiehuan
 * Email: 1487471733@qq.com
 * Date: 2017/2/27
 * Time: 14:39
 * Describe:
 */

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapper tagMapper;

    //利用pageHelper分页查询标签
    @Override
    public JsonResult findAllTagsCT(Integer id) {
        PageHelper.startPage(id,6);
        List<Tag> findAllTagCT=tagMapper.selectAllTagsInfos();
        PageInfo<Tag> pageInfo=new PageInfo<>(findAllTagCT);
        JsonResult jsonResult = new JsonResult();

        jsonResult.setObj(pageInfo);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    @Override
    public int insert(Tag record) {
        int i=tagMapper.insert(record);
        return i;
    }

    @Override
    public int updateByPrimaryKeySelective(Tag record) {
        int i=tagMapper.updateByPrimaryKeySelective(record);
        return i;
    }

    @Override
    public int deleteTag(Integer id) {
        int i=tagMapper.deleteTag(id);
        return i;
    }
}
