package me.teacms.service.impl;

import me.teacms.common.result.JsonResult;
import me.teacms.common.utils.AuthUtil;
import me.teacms.dao.UserMapper;
import me.teacms.entity.User;
import me.teacms.entity.vo.UserVo;
import me.teacms.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/1
 * Time: 23:51
 * Describe: 用户登陆-用户注销
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 查询用户密码是否正确
     * @param userVo
     * @return
     */
    @Override
    public User findUserPassIsCorrect(UserVo userVo) {

        userVo.setUserPass(AuthUtil.getPassword(userVo.getUserPass()));
        User userPassIsCorrect = userMapper.findUserPassIsCorrect(userVo);

        return userPassIsCorrect;
    }

    @Override
    public JsonResult updateUserPass(HttpSession session, JsonResult jsonResult, User user, String psaa2) {
        String password = AuthUtil.getPassword(psaa2);
        user.setUserPass(password);
        int i = userMapper.updateByPrimaryKeySelective(user);
        if (i>=1) {
            session.setAttribute("user", user);
            jsonResult.setSuccess(true);
            jsonResult.setMsg("修改成功!");
        } else {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("修改失败!");
        }
        return jsonResult;
    }
}
