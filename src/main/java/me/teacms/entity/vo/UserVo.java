package me.teacms.entity.vo;

import me.teacms.entity.User;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/2
 * Time: 0:22
 * Describe: 用户类 Entity 增强--增加验证码
 */
public class UserVo extends User {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
